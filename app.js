//const axios = require('axios');

const lugar = require('./lugar/lugar');

const argv = require('yargs').options({
    direccion: {
        alias: 'd',
        desc: 'Descirpción de la ciudad para obtener el clima',
        demand: true
    }
}).argv;

//console.log(argv.direccion);

lugar.getLugarLatLng(argv.direccion).then(respuesta => {
    console.log(respuesta);
}).catch (error => {
    console.log(error);
})