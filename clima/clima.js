
const axios = require('axios');

const  getClima = async (lat,lng) => {

    let respuesta =  await axios.get(`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lng}&units=metric&appid=ecfe11c523d6cd96b548c1773ac65e06`);
    let valores = respuesta.data.main;
    return valores;
}


module.exports = {
    getClima
}