const axios = require('axios');

const clima = require('../clima/clima');



const getLugarLatLng = async (direccion) => {

    let Uridireccion = encodeURI(direccion);

   let respuesta =  await axios.get(`https://maps.googleapis.com/maps/api/geocode/json?address=${Uridireccion}&key=AIzaSyAu2rb0mobiznVJnJd6bVb5Bn2WsuXP2QI`);

   if(respuesta.data.status === "ZERO_RESULTS"){
       throw new Error (`No hemos podido obtener información sobre la dirección ingresada ${direccion}`);
   }

   let location = respuesta.data.results[0];
   let coors = location.geometry.location;
    
   const climaactual = await clima.getClima(coors.lat, coors.lng).then(respuesta => {
      //var temperatura = respuesta;
     // console.log(`Aqui la temperatura ${temperatura}`);
       return respuesta.temp;
  })

  //console.log (await climaactual);
  

   return {
        direccion: location.formatted_address,
        lat: coors.lat,
        lng: coors.lng,
        grados: climaactual
    }
}
//console.log();


module.exports = {
    getLugarLatLng
}
